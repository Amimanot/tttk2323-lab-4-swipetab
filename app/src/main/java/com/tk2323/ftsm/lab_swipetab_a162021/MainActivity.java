package com.tk2323.ftsm.lab_swipetab_a162021;

import android.content.Intent;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.main_toolbar);
        tabLayout = findViewById(R.id.main_tabs);
        viewPager = findViewById(R.id.main_viewpager);

        setupToolbar();
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition())
                {
                    case 0:
                        Toast.makeText(getApplicationContext(), "Fragment A is selected", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        Toast.makeText(getApplicationContext(), "Fragment B is selected", Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        Toast.makeText(getApplicationContext(), "Fragment C is selected", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    private void setupToolbar()
    {
        setSupportActionBar(toolbar);
        final ActionBar myActionBar = getSupportActionBar();
        myActionBar.setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId())
        {
            case R.id.action_menu_search:
                Intent searchIntent = new Intent(MainActivity.this, SearchActivity.class);
                Toast.makeText(MainActivity.this, "Search is clicked", Toast.LENGTH_SHORT).show();
                startActivity(searchIntent);
                return true;

            case R.id.action_menu_sync:
                Intent syncIntent = new Intent (MainActivity.this, SyncActivity.class);
                Toast.makeText(MainActivity.this, "Sync is clicked", Toast.LENGTH_SHORT).show();
                startActivity(syncIntent);
                return true;

            case R.id.action_menu_settings:
                Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
                Toast.makeText(MainActivity.this, "Settings is clicked", Toast.LENGTH_SHORT).show();
                startActivity(settingsIntent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void setupViewPager (ViewPager viewPager)
    {
        TabViewPagerAdapter adapter = new TabViewPagerAdapter(getSupportFragmentManager());

        adapter.addFrag(new FragmentA(), "Tab A");
        adapter.addFrag(new FragmentB(), "Tab B");
        adapter.addFrag(new FragmentC(), "Tab C");

        viewPager.setAdapter(adapter);
    }
}
